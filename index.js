const axios = require('axios')
const numeral = require('numeral')

const URL = 'https://exchangeratesapi.io/api/'

const ERROR = { error: 'Cannot exchange currency, wrong input data' }

/**
 * Get currency exchange information using 3-party API
 * @param String date
 * @param String base 
 * @param Number amount
 * @param String conversion
 * @return currency information
 */
module.exports = function(date, base, amount, conversion) {
    return new Promise((resolve, reject) => {
        // if amount is not a number or coversion is null, return error directly
        if (isNaN(amount) || conversion == null) return reject(ERROR)
        requestRateByDateAndBase(date, base)
            .then(response => {
                // If base equals to conversion, return same amount; Otherwise caculate amount
                let conversionAmount = getConversionAmount(response.data.base, amount, conversion, response.data.rates[conversion.toUpperCase()])
                if (conversionAmount == null) return reject(ERROR)
                resolve({ date: date ? date : response.data.date, base_currency: response.data.base, base_amount: amount, conversion_currency: conversion.toUpperCase(), conversion_amount: conversionAmount })
            })
            .catch(error => {
                //Log error, return general error
                reject(ERROR)
            })
    })
}

/**
 * Generate request
 * @param string date
 * @param string base
 * @return promise
 */
function requestRateByDateAndBase(date, base) {
    // Build reuqest URL due to exchange data
    // If number of currencies are too many, then use symbols parameter in URL
    let requestUrl = URL + (date == null ? 'latest?' : date + '?') + (base ? '&base=' + base.toUpperCase() : '')
    return axios.get(requestUrl)
}

/**
 * Caculate amount of currency conversion
 * @param Number amount
 * @param String rate
 * @return Number
 */
function getConversionAmount(base, amount, conversion, rate) {
    // If rate not exist return null
    if (rate == null) return null
    // If base and conversion are same, return same amount
    if (conversion != null && base.toUpperCase() === conversion.toUpperCase()) return amount
    return numeral(amount).multiply(rate).value()
}