const foreignExchange = require('../index.js')

foreignExchange('2017-06-03', 'USD', 100, 'CAD')
    .then(response => {
        console.log(response)
    })
    .catch(error => {
        console.log(error)
    })

foreignExchange('2007-07-12', 'GBP', 303, 'SEK')
    .then(response => {
        console.log(response)
    })
    .catch(error => {
        console.log(error)
    })

foreignExchange('2004-08-07', 'EUR', 5, 'PLN')
    .then(response => {
        console.log(response)
    })
    .catch(error => {
        console.log(error)
    })

foreignExchange('2011-06-20', null, 100, 'CAD')
    .then(response => {
        console.log(response)
    })
    .catch(error => {
        console.log(error)
    })