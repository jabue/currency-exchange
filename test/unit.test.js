const assert = require('assert')
const nock = require('nock')
const foreignExchange = require('../index')

// All test data
const testData = require('./unit.test.data')

const BASE_URL = 'https://exchangeratesapi.io'


describe('Promise test exchange currency', () => {

    testData.forEach(test => {

        nock(BASE_URL)
            .get(test.mock.url)
            .query({
                base: test.mock.response.base
            })
            .reply(200, test.mock.response)

        it(test.message, () => {
            foreignExchange.apply(null, test.args)
                .then(data => {
                    assert.deepEqual(data, test.result)
                })
                .catch(error => {
                    assert.deepEqual(error, test.error)
                })
        })
    })
})