module.exports = [{
        message: 'Test correct input data 1',
        args: ['2011-06-03', 'USD', 100, 'CAD'],
        result: {
            date: "2011-06-03",
            base_currency: "USD",
            base_amount: 100,
            conversion_currency: "CAD",
            conversion_amount: 97.85
        },
        error: null,
        mock: {
            url: '/api/2011-06-03',
            response: {
                base: 'USD',
                date: '2011-06-03',
                rates: {
                    CAD: 0.9785
                }
            }
        }
    },
    {
        message: 'Test correct input data 2',
        args: ['2007-07-12', 'GBP', 303, 'SEK'],
        result: {
            date: "2007-07-12",
            base_currency: "GBP",
            base_amount: 303,
            conversion_currency: "SEK",
            conversion_amount: 4085.0157
        },
        error: null,
        mock: {
            url: '/api/2007-07-12',
            response: {
                base: 'GBP',
                date: '2007-07-12',
                rates: {
                    SEK: 13.4819
                }
            }
        }
    },
    {
        message: 'Test correct input data 3',
        args: ['2004-08-07', 'EUR', 5, 'PLN'],
        result: {
            date: "2004-08-07",
            base_currency: "EUR",
            base_amount: 5,
            conversion_currency: "PLN",
            conversion_amount: 22.01
        },
        error: null,
        mock: {
            url: '/api/2004-08-07',
            response: {
                base: 'EUR',
                date: '2004-08-07',
                rates: {
                    PLN: 4.402
                }
            }
        }
    },
    {
        message: 'Test correct input data 4',
        args: ['2017-02-09', 'ZAR', 132, 'TRY'],
        result: {
            date: "2017-02-09",
            base_currency: "ZAR",
            base_amount: 132,
            conversion_currency: "TRY",
            conversion_amount: 36.3528
        },
        error: null,
        mock: {
            url: '/api/2017-02-09',
            response: {
                base: 'ZAR',
                date: '2017-02-09',
                rates: {
                    TRY: 0.2754
                }
            }
        }
    }
]