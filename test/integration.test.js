const express = require('express')
const assert = require('assert')
const nock = require('nock')
const request = require('request')
const foreignExchange = require('../index.js')

// All test data
const testData = require('./integration.test.data')
const app = express()
const BASE_URL = 'https://exchangeratesapi.io'
const URL = 'http://localhost:3000/exchange'
const PORT = 3000
let server

describe('Intergration test request currency exchange', function() {

    before((done) => {
        app.get('/exchange', (req, res) => {
            foreignExchange.apply(null, req.query.args)
                .then(response => {
                    res.send(response)
                })
                .catch(error => {
                    res.send(error)
                })
        })
        server = app.listen(PORT, () => done())
    })

    after((done) => {
        server.close(done)
    })

    testData.forEach(test => {

        nock(BASE_URL)
            .get(test.mock.url)
            .query({
                base: test.mock.response.base
            })
            .reply(200, test.mock.response)

        it(test.message, () => {
            request({
                url: URL,
                qs: {
                    args: test.args
                }
            }, function(error, response, body) {
                if (test.result) assert.deepEqual(JSON.parse(body), test.result)
            })
        })
    })
})