# Currency Exchange

Conversion currency using historical rate. Based on [exchangeratesapi.io](https://exchangeratesapi.io/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Install dependencies

```
npm install
```

### Usage example

```
npm run examples
```

```
const foreignExchange = require('../index.js')
```

```
foreignExchange('2017-06-03', 'USD', 100, 'CAD')
    .then(response => {
        console.log(response)
    })
    .catch(error => {
        console.log(error)
    })
```

```
/**
 * @param String date format yyyy-mm-dd
 * @param String base
 * @param Number amount
 * @param String conversion
 * @return currency information
 */
foreignExchange(date, base, amount, conversion);
```

## Running the tests

### Unit test

```
npm run test-unit
```

### Integration test

```
npm run test-integration
```

## Bundle With 

* [browserify](http://browserify.org/)

## Authors

* **Chris Yang** - *Initial work* - [Jabue](https://bitbucket.org/jabue/)
